# Urbansprawl

The urbansprawl project provides an open framework to assess the urban sprawl phenomenon.
It uses OpenStreetMap data to calculate its sprawling indices, divided in Accessibility, Land use mix, and Dispersion.

**The repository has been moved to GitHub. Please refer to the following link:**

* [Urbansprawl repository](https://github.com/lgervasoni/urbansprawl)